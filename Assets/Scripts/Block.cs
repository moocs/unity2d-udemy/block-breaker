﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    // Config parameters
    [SerializeField] private AudioClip breakSound;
    [SerializeField] private GameObject blockSparklesVfx;
    [SerializeField] private Sprite[] hitSprites;

    // Cached references
    private Level _level;
    private GameSession _gameSession;

    // State variables
    private int _timesHit;
    private int _maxHits;

    private void Start()
    {
        CountBreakableBlocks();
        _maxHits = hitSprites.Length + 1;
        _gameSession = FindObjectOfType<GameSession>();
    }

    private void CountBreakableBlocks()
    {
        _level = FindObjectOfType<Level>();
        if (CompareTag("Breakable"))
        {
            _level.addOneBlock();
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (CompareTag("Breakable"))
        {
            HandleHit();
        }
    }

    private void HandleHit()
    {
        _timesHit++;
        if (_timesHit >= _maxHits)
        {
            DestroyBlock();
        }
        else
        {
            ShowNextHitSprite();
        }
    }

    private void ShowNextHitSprite()
    {
        var spriteRenderer = GetComponent<SpriteRenderer>();
        var spriteIndex = _timesHit - 1;
        if (hitSprites[spriteIndex] != null)
        {
            spriteRenderer.sprite = hitSprites[spriteIndex];
        }
        else
        {
            Debug.LogError("Block Sprite is missing from array");
        }
    }

    private void DestroyBlock()
    {
        BlockDestroySoundEffect();
        _level.removeOneBlock();
        _gameSession.AddPointsBlockDestroyed(_maxHits);
        TriggerSparklesVfx();
        Destroy(gameObject);
    }

    private void BlockDestroySoundEffect()
    {
        var mainCamera = Camera.main;
        if (mainCamera == null) return;
        var soundPosition = mainCamera.transform.position; // Or transform.position if placed on the block.
        AudioSource.PlayClipAtPoint(breakSound, soundPosition);
    }

    private void TriggerSparklesVfx()
    {
        var transform1 = transform;
        var sparkles = Instantiate(blockSparklesVfx, transform1.position, transform1.rotation);
        Destroy(sparkles, 1f);
    }
}