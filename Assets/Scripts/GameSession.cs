﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameSession : MonoBehaviour
{
    // Config
    [Range(0.1f, 10f)] [SerializeField] private float gameSpeed = 1f;
    [SerializeField] private int pointsPerBlockDestroyed = 50;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private bool autoPlayEnabled;

    // State variables
    [SerializeField] private int currentScore = 0;


    private void Awake()
    {
        int gameStatusCount = FindObjectsOfType<GameSession>().Length;
        if (gameStatusCount > 1)
        {
            // If there are already existing gameStatus, destroy yourself as you're not the first one
            GameObject o;
            (o = gameObject).SetActive(false);
            Destroy(o);
        }
        else
        {
            // Otherwise, you're the first : you can exist and you should not get destroyed at the end of the Scene
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        UpdateTextSCore();
    }

    // Update is called once per frame
    void Update()
    {
        Time.timeScale = gameSpeed;
    }

    public void AddPointsBlockDestroyed(int numberOfHits)
    {
        AddPointsToScore(pointsPerBlockDestroyed * numberOfHits);
    }

    private void AddPointsToScore(int pointsToAdd)
    {
        currentScore += pointsToAdd;
        UpdateTextSCore();
    }

    private void UpdateTextSCore()
    {
        scoreText.text = currentScore.ToString();
    }

    public void ResetGame()
    {
        Destroy(gameObject);
    }

    public bool IsAutoPlayEnabled()
    {
        return autoPlayEnabled;
    }
}