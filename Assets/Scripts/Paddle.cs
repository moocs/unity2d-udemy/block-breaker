﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    // Configurations variables
    [SerializeField] private float screenWidthInUnits = 16;
    [SerializeField] private float minX = 1f;
    [SerializeField] private float maxX = 15f;

    private GameSession _gameSession;
    private Ball _ball;

    // Start is called before the first frame update
    void Start()
    {
        _gameSession = FindObjectOfType<GameSession>();
        _ball = FindObjectOfType<Ball>();
    }

    // Update is called once per frame
    void Update()
    {
        var paddleTransform = transform;
        var newHorizPos = Mathf.Clamp(GetHorizPos(), minX, maxX);
        var paddlePos = new Vector2(newHorizPos, paddleTransform.position.y);
        paddleTransform.position = paddlePos;
    }

    private float GetHorizPos()
    {
        if (_gameSession.IsAutoPlayEnabled())
        {
            return _ball.transform.position.x;
        }

        return Input.mousePosition.x / Screen.width * screenWidthInUnits;
    }
}