﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void LoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex + 1);
    }

    public void LoadInitialScene()
    {
        ResetGame();
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    private static void ResetGame()
    {
        SceneManager.LoadScene(0);
        var gameStatus = FindObjectOfType<GameSession>();
        gameStatus.ResetGame();
    }
}