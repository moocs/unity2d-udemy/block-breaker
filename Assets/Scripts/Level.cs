﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    private int _blockCount = 0; // Initialised to 0 automatically

    private SceneLoader _sceneLoader;

    private void Start()
    {
        _sceneLoader = FindObjectOfType<SceneLoader>();
    }

    public void addOneBlock()
    {
        _blockCount++;
    }

    public void removeOneBlock()
    {
        _blockCount--;
        if (_blockCount <= 0)
        {
            _sceneLoader.LoadNextScene();
        }
    }
}